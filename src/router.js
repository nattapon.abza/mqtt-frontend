import Vue from 'vue'
import Router from 'vue-router'
import DashboardLayout from '@/layout/DashboardLayout'
import AuthLayout from '@/layout/AuthLayout'
Vue.use(Router)

export default new Router({
    linkExactActiveClass: 'active',
    routes: [{
            path: '/',
            redirect: 'login',
            component: DashboardLayout,
            children: [{
                    path: '/manage',
                    name: 'mange users',
                    // route level code-splitting
                    // this generates a separate chunk (about.[hash].js) for this route
                    // which is lazy-loaded when the route is visited.
                    component: () =>
                        import ( /* webpackChunkName: "demo" */ './views/ManageUser.vue')
                },
                {
                    path: '/token',
                    name: 'token',
                    component: () =>
                        import ( /* webpackChunkName: "demo" */ './views/GenToken.vue')
                },
                {
                    path: '/topic',
                    name: 'topic',
                    component: () =>
                        import ( /* webpackChunkName: "demo" */ './views/topic/TopicCompany.vue')
                },
                {
                    path: '/alltopic',
                    name: 'alltopic',
                    component: () =>
                        import ( /* webpackChunkName: "demo" */ './views/topic/TopicAll.vue')
                },
                {
                    path: '/app',
                    name: 'app',
                    component: () =>
                        import ( /* webpackChunkName: "demo" */ './views/topic/TopicApp.vue')
                },
                {
                    path: '/hotel',
                    name: 'hotel',
                    component: () =>
                        import ( /* webpackChunkName: "demo" */ './views/topic/TopicHotel.vue')
                },
                {
                    path: '/buiding',
                    name: 'buiding',
                    component: () =>
                        import ( /* webpackChunkName: "demo" */ './views/topic/TopicBuiding.vue')
                },
                {
                    path: '/floor',
                    name: 'floor',
                    component: () =>
                        import ( /* webpackChunkName: "demo" */ './views/topic/TopicFloor.vue')
                },
                {
                    path: '/room',
                    name: 'room',
                    component: () =>
                        import ( /* webpackChunkName: "demo" */ './views/topic/TopicRoom.vue')
                },
                {
                    path: '/example',
                    name: 'example',
                    component: () =>
                        import ( /* webpackChunkName: "demo" */ './views/topic/TopicExample.vue')
                },
                {
                    path: '/profile',
                    name: 'profile',
                    component: () =>
                        import ( /* webpackChunkName: "demo" */ './views/UserProfile.vue')
                },
                {
                    path: '/manage/profile/:user_id',
                    name: 'profile users',
                    component: () =>
                        import ( /* webpackChunkName: "demo" */ './views/ManageUserProfile.vue')
                },
                {
                    path: '/manage/change/:user_id',
                    name: 'chang password users',
                    component: () =>
                        import ( /* webpackChunkName: "demo" */ './views/ManageChangePassword.vue')
                },
                {
                    path: '/manage/create',
                    name: 'create user',
                    component: () =>
                        import ( /* webpackChunkName: "demo" */ './views/ManageCreateUser.vue')
                },
                {
                    path: '/change',
                    name: 'change password',
                    component: () =>
                        import ( /* webpackChunkName: "demo" */ './views/ChangePassword.vue')
                }
            ]
        },
        {
            path: '/',
            redirect: 'login',
            component: AuthLayout,
            children: [{
                    path: '/login',
                    name: 'login',
                    component: () =>
                        import ( /* webpackChunkName: "demo" */ './views/Login.vue')
                },
                {
                    path: '/register',
                    name: 'register',
                    component: () =>
                        import ( /* webpackChunkName: "demo" */ './views/Register.vue')
                },
                {
                    path: '/logout',
                    name: 'logout',
                    component: () =>
                        import ( /* webpackChunkName: "demo" */ './views/Logout.vue')
                }
            ]
        }
    ]
})