import axios from 'axios'

const API_URL = process.env.API_URL || 'http://localhost:8000'

export default axios.create({
    baseURL: API_URL,
    headers: {
        'Authorization': 'Bearer ' + localStorage.token,
        'Content-Type': 'application/json; charset=utf-8',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS'
    }
})